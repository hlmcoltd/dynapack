var subCatContainer = $(".inside-table-y");

subCatContainer.scroll(function() {
  subCatContainer.scrollTop($(this).scrollTop());
});

$(".bat-off").click(function() {
  $(".bat-off").removeClass("bat-on");
  $(this).addClass("bat-on");
});

$(".ser-btn").click(function() {
  $(".ser-btn").removeClass("ser-btn-on");
  $(this).addClass("ser-btn-on");
});

// table click
var _gaq = _gaq || [];
_gaq.push(["_setAccount", "UA-36251023-1"]);
_gaq.push(["_setDomainName", "jqueryscript.net"]);
_gaq.push(["_trackPageview"]);

(function() {
  var ga = document.createElement("script");
  ga.type = "text/javascript";
  ga.async = true;
  ga.src =
    ("https:" == document.location.protocol ? "https://ssl" : "http://www") +
    ".google-analytics.com/ga.js";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(ga, s);

  $("tbody tr").click(function() {
    console.log($(this).index(), $("table").length);

    var trIndex = $(this).index();

    $("table").each(function(index) {
      $(this)
        .find("tr")
        .each(function() {
          $(this).removeClass("highlight");
        });

      var indexControl = index == 3 ? trIndex : trIndex + 1;

      console.log($($(this).find("tr")[trIndex]).html());

      var tr = $($(this).find("tr")[indexControl]);

      var selected = tr.hasClass("highlight");

      console.log(selected);

      tr.removeClass("highlight");
      if (!selected) tr.addClass("highlight");
    });
  });
})();
