function resize(){
    var layoutH = $('.layout').height();
    // console.log(layoutH, document.body.offsetHeight);
    if(layoutH < document.body.offsetHeight){
        $('footer').css('position', 'fixed');
        $('footer').css('bottom', 0);
    } else {
        $('footer').css('position', 'static');
    }
}

$(window).resize(resize);

$(function(){
    $('body').css('background', '#00001d');
    resize();
});